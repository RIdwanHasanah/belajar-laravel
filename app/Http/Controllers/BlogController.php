<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; //untuk menggunakan database di laravel

use App\Models\Blog; //untuk menggunakan model Blog


class BlogController extends Controller
{
    public function index(){ //function yg di panggil di routes  web.php


    $blogs = Blog::all(); // all() mengambil semua isi dari Model Blog

    	return view('blog/home',['blogs' => $blogs]); //mengambil tampilan dari resource/view/blog/home.blade.php
    }

    public function show($id){ //function yg di panggil di routes  web.php

        //$blog = Blog::findorFail($id); //or faile untuk menampikan bahwa id yg di klik tidak ditemukan
        $blog = Blog::find($id);

        if (!$blog) {

            abort(404); // abort untuk menampilkan halaman error yang ada di 
        }
        


        return view('blog/single',['blog'=>$blog]); 
    	//mengambil tampilan dari resource/view/blog/home.blade.php
    	//jika ingin mengoper lebih dari satu data cuku[ di tambahkan koma]
    }
    /*CREATE*/

    public function create(){
        return view('blog/create');
    }



    public function store(Request $request){

        /*========== Validasi Start ==========*/
        $this->validate($request,[

            'title' => 'required|min:5',
            'description' => 'required|min:5'

        ]);
        /*========== Validasi END ==========*/


        $blog = new Blog;
        $blog->title = $request->title;
        $blog->description = $request->description;
        $blog->save();

        return redirect('blog');
    }


    /*========= EDIT and UPDATE Strat ========*/
    public function edit($id){

        $blog = Blog::find($id);

        if (!$blog) {

            abort(404); // abort untuk menampilkan halaman error yang ada di 
        }
        
        return view('blog/edit',['blog'=>$blog]); 

    }

    public function update(Request $request,$id){

        $blog = Blog::find($id);
        $blog->title       = $request->title;
        $blog->description = $request->description;

        $blog->save();
        return redirect('blog/' . $id);

    }
    /*========= EDIT and UPDATE END ========*/

    public function destroy($id){

        $blog = Blog::find($id);
        $blog->delete();

        return redirect('blog');
    }



}
