<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//SoftDeletes untuk menghapus data pada tampilan tapi tidak di database


class Blog extends Model {

	use SoftDeletes;

	protected $dates = ['deleted_at']; 

	//protected $tabel = 'blog'; //gunakan ini jika nama table tidak plural 

    //public $timestamps = false; //created_at dan updated_at tidak ada

    /*white list untuk field mana saja yg boleh di isi*/
    //protected $fillable = ['title','description'];

    //black list untuk field yang tidak boleh di isi
    protected $guarded = ['created_at']; 

}
