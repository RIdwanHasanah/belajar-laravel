<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { 
    return view('welcome');
});
Route::get('/blog','BlogController@index'); //utk memanggil function index


Route::get('/blog/create','BlogController@create'); //utk memanggil function edit dengan menerima parameter
Route::post('/blog','BlogController@store');

Route::get('/blog/{id}','BlogController@show'); //utk memanggil function show atau single page dengan menerima parameter


/*Edit*/
Route::get('/blog/{id}/edit','BlogController@edit'); //utk memanggil function edit dengan menerima parameter
Route::put('/blog/{id}','BlogController@update');

/*DELETE*/
Route::delete('/blog/{id}','BlogController@destroy');
