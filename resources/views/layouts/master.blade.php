<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="/css/master.css">
</head>
<body>
	<div class="header">
	<header>
		<nav>
			<a href="/">Home</a>
			<a href="/">Contact</a>
			<a href="/">Testimonial</a>
			<a href="/">Profile</a>
			<a href="/">Gallery</a>
			<a href="/blog">Blog</a>
		</nav>
	</header>
	</div>
	<br>

	@yield('content')

	<footer>
		<p>
			&copy;  Ridwan Hasnah belajar Laravel
		</p>
	</footer>
	<script type="text/javascript" src="/js/main.js"></script>
</body>
</html>